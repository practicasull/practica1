#include "Grafo.h"

Grafo::Grafo()
{
}

Grafo::~Grafo()
{
}

Grafo::Grafo(std::vector<unsigned int> v)
{
    for (unsigned int i=0;i<v.size();++i) {


        Nodo N_aux(v[i]);

        std::pair<Nodo,std::vector<unsigned int>> aux;

        aux.first = N_aux;

        v_.push_back(aux);

    }
}


Grafo::Grafo(std::vector<unsigned int> v, unsigned int suma)
{
    this->setSuma_(suma);

    for (unsigned int i=0;i<v.size();++i) {


        Nodo N_aux(v[i]);

        std::pair<Nodo,std::vector<unsigned int>> aux;

        aux.first = N_aux;

        v_.push_back(aux);

    }

}

bool Grafo::operator<(const Grafo& rhs) const
{

    if ( suma_ == rhs.suma_ ) {

        std::string entero_this;
        std::string entero_rhs;

        for (unsigned int i=0;i<v_.size();++i) {

            entero_this += std::to_string(v_[i].first.getVal_());

        }

        for (unsigned int i=0;i<rhs.v_.size();++i) {

            entero_rhs += std::to_string(rhs.v_[i].first.getVal_());

        }

        return ( std::stoul(entero_this) < std::stoul(entero_rhs) );    //string to unsigned long


    }
    return suma_ < rhs.suma_;
}

bool Grafo::operator>(const Grafo& rhs) const
{
    return rhs < *this;
}

bool Grafo::operator<=(const Grafo& rhs) const
{
    return !(rhs < *this);
}

bool Grafo::operator>=(const Grafo& rhs) const
{
    return !(*this < rhs);
}

unsigned int Grafo::getSuma_() const
{
    return suma_;
}

void Grafo::setSuma_(unsigned int suma_)
{
    Grafo::suma_ = suma_;
}

std::ostream& operator<<(std::ostream& os, const Grafo& grafo)
{
    os << grafo.getSuma_() << "     ";

    int cnt = 0;

    for (unsigned int j = 0; j < grafo.v_.size(); ++j) {

        os << grafo.v_[j].first.getVal_();

        if (cnt == 2) {

            os << ";";
            cnt = 0;

        }
        else {

            os << ",";
            cnt++;

        }
    }

    os << std::endl;
    return os;
}

bool Grafo::operator==(const Grafo& rhs) const
{
    return v_ == rhs.v_;
}

bool Grafo::operator!=(const Grafo& rhs) const
{
    return !(rhs == *this);
}

