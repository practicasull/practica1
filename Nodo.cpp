#include "Nodo.h"

Nodo::Nodo()
{

}

Nodo::~Nodo()
{

}

Nodo::Nodo(unsigned int val):

val_(val)
{}

Nodo::Nodo(const Nodo& cpy)
{
    this->val_ = cpy.getVal_();
}

unsigned int Nodo::getVal_() const
{
    return val_;
}

void Nodo::setVal_(unsigned int id_)
{
    Nodo::val_ = id_;
}

void Nodo::show()
{
    std::cout << this->getVal_() << " ";
}

bool Nodo::operator==(const Nodo& rhs) const
{
    return val_ == rhs.val_;
}

bool Nodo::operator!=(const Nodo& rhs) const
{
    return !(rhs == *this);
}

bool Nodo::operator<(const Nodo& rhs) const
{
    return val_ < rhs.val_;
}

bool Nodo::operator>(const Nodo& rhs) const
{
    return rhs < *this;
}

bool Nodo::operator<=(const Nodo& rhs) const
{
    return !(rhs < *this);
}

bool Nodo::operator>=(const Nodo& rhs) const
{
    return !(*this < rhs);
}
