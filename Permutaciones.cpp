#include "Permutaciones.h"

Permutaciones::Permutaciones()
{}

Permutaciones::Permutaciones(const Permutaciones& cpy)
{

    this->g_ = cpy.g_;

}

Permutaciones::Permutaciones(unsigned int n):
n_(n)
{}

Permutaciones::~Permutaciones()
{

}

void Permutaciones::hallar_solucion()
{

    std::vector<unsigned int> v_;

    for (unsigned int i=1;i<=n_;++i)

        v_.push_back(i);

    // Iteradores para hacer las permutaciones con un vector de la STL

    std::vector<unsigned int>::iterator begin_ = v_.begin();
    std::vector<unsigned int>::iterator end_ = v_.end();

    do {


        std::vector<unsigned int> int_;
        std::vector<unsigned int> ext_;
        std::vector<unsigned int> sol_act;

        // Extrayendo los nodos exteriores e interiores a vectores distintos
        for (unsigned int i=0;i<v_.size()/2;++i) {

            int_.push_back(v_[i]);
            ext_.push_back(v_[i+v_.size()/2]);

        }

        // Construyedo solucion a partir de los vectores de nodos exteriores e interiores
        for (unsigned int i=0;i<int_.size();++i) {

            if ( i!=int_.size()-1 ) {

                sol_act.push_back(ext_[i]);
                sol_act.push_back(int_[i]);
                sol_act.push_back(int_[i+1]);

            }
            else {

                sol_act.push_back(ext_[i]);
                sol_act.push_back(int_[i]);
                sol_act.push_back(int_[0]);

            }

        }

        // Comprobando si es solucion

        bool is_solution = true;

        unsigned int i=0;
        unsigned int n;
        unsigned int suma = 0;

        while ( i<sol_act.size() ) {

            unsigned int suma_aux = 0;

            n = i+3;

            for (i; i<n; ++i) {

                suma_aux += sol_act[i];

            }
            if ( i==3 ) {

                suma = suma_aux;

            }
            if ( suma_aux != suma ) {

                is_solution = false;
                break;
            }
        }

        if ( is_solution ) {

            std::vector<unsigned int> V = this->ordenar_solucion(sol_act);

            Grafo G(V, suma);

            g_.insert(G);

        }

    } while ( std::next_permutation(begin_,end_) );

}

std::vector<unsigned int> Permutaciones::ordenar_solucion(std::vector<unsigned int> sol)
{
    std::vector<unsigned int> aux;

    unsigned int min = MAX;
    unsigned int pos = 0;

    for (unsigned int i=0;i<sol.size();i=i+3) {

        if ( sol[i] < min ) {

            pos = i;
            min = sol[i];

        }

    }

    for (unsigned int i=pos;i<sol.size();++i)

        aux.push_back(sol[i]);

    for (unsigned int i=0;i<pos;++i)

        aux.push_back(sol[i]);


    return aux;
}

void Permutaciones::mostrar_solucion()
{

    for (std::set<Grafo>::iterator it=g_.begin();it!=g_.end();it++) {

        std::cout << (*it);
    }

}

unsigned int Permutaciones::comprobar_solucion(const Grafo& G)
{
    unsigned int pos = 0;

    for (std::set<Grafo>::iterator it=g_.begin();it!=g_.end();it++) {

        if ( (*it) == G )   break;
        else    pos++;

    }

    if ( pos == g_.size() )

        return MAX;

    else

        return pos;
}
