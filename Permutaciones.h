#pragma once

#define MAX 100000

#include "Grafo.h"

#include <iostream>
#include <algorithm>
#include <set>

class Permutaciones
{

private:

    std::set<Grafo> g_;
    unsigned int n_;        // numero de nodos

public:

    Permutaciones();
    Permutaciones(unsigned int n);
    Permutaciones(const Permutaciones &cpy);
    ~Permutaciones();

    void hallar_solucion();     // mediante permutaciones

    void mostrar_solucion();

    unsigned int comprobar_solucion(const Grafo &G);    // usuario introduce sol. y se comprueba

private:

    std::vector<unsigned int> ordenar_solucion(std::vector<unsigned int> sol);  // para que la primera parte de la solucion sea encabezada por el nodo más pequeño y luego siga las agujas del reloj

};