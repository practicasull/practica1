#pragma once

#include <vector>
#include <iostream>

class Nodo
{

private:

    unsigned int val_;          // Identificador del nodo

public:

    Nodo();
    Nodo(unsigned int val);
    Nodo(const Nodo& cpy);
    ~Nodo();

    unsigned int getVal_() const;

    void setVal_(unsigned int id_);

    void show();

    bool operator==(const Nodo& rhs) const;

    bool operator!=(const Nodo& rhs) const;

    bool operator<(const Nodo& rhs) const;

    bool operator>(const Nodo& rhs) const;

    bool operator<=(const Nodo& rhs) const;

    bool operator>=(const Nodo& rhs) const;
};