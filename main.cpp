#include <iostream>
#include "Permutaciones.h"


/** Notas de Implementacion: Requisitos minimos
 * - Solicitar al usuario el numero de nodos del grafo
 * - Listar las posibles soluciones para el tamaño de grafo especificado
 * - Listar de forma ordenada los valores enteros de las cadenas formadas a partir de las soluciones obtenidas
 * - Mostrar el mayor valor entero obtenido de las cadenas solucion y visualizar la solucion que lo produce
 * - Permitir que el usuario complete las etiquetas de los nodos de forma manual y comprobar so la solucion especificada
 *      los requisitos del problema, en caso afirmativo indicar en que posicion se encuentra con respecto a los valores
 *      numericos representados pos la cadena
 *
 */

int main()
{

    Permutaciones P(10);
    P.hallar_solucion();
    P.mostrar_solucion();

    // Introduccion manual de las etiquetas

    std::string solucion;

    std::cout << "Introduzca una solucion: ";
    std::cin >> solucion;           // a,b,c;d,e,f;g,h,i;

    std::string aux;

    std::vector<unsigned int> v_;

    unsigned int cnt = 0;

    unsigned long pos = 0;

    bool reset_cnt = false;

    while ( !solucion.empty() ) {

        if ( cnt != 2 ) {

            pos = solucion.find(",");
            reset_cnt = false;

        }

        else {

            pos = solucion.find(";");
            reset_cnt = true;

        }

        if ( pos!=std::string::npos ) {

            aux = solucion.substr(0, pos);
            solucion.erase(0, pos+1);
            cnt++;
        }

        else

            aux = solucion;


        v_.push_back(stoul(aux));

        std::cout << solucion << "     " << aux << std::endl;

        if (reset_cnt)
            cnt = 0;


    }

    Grafo G(v_);

    unsigned int posicion = P.comprobar_solucion(G);

    if ( posicion != MAX )

        std::cout << "Sí es solución, y es en la posición " << posicion << std::endl;

    else

        std::cout << "No es solucion para el numero de nodos especificado" << std::endl;

}
