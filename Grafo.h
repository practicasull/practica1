#pragma once

#include "Nodo.h"

#include <vector>
#include <algorithm>
#include <ostream>
#include <string>

class Grafo
{

private:

    std::vector< std::pair< Nodo, std::vector<unsigned int>>>  v_;       // Vector que contiene un nodo y su lista de adyacencia en la misma posición
    unsigned int suma_;

public:

    Grafo();
    Grafo(std::vector<unsigned int> v);
    Grafo(std::vector<unsigned int> v, unsigned int suma);
    ~Grafo();

    unsigned int getSuma_() const;

    void setSuma_(unsigned int suma_);


    bool operator<(const Grafo& rhs) const;

    bool operator>(const Grafo& rhs) const;

    bool operator<=(const Grafo& rhs) const;

    bool operator>=(const Grafo& rhs) const;

    bool operator==(const Grafo& rhs) const;

    bool operator!=(const Grafo& rhs) const;

    friend std::ostream& operator<<(std::ostream& os, const Grafo& grafo);


};

